import "./App.css";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
import Form from "./components/Form";

function App() {
  return (
    <div className="App">
      <div className="Header">
        <Header />
        <Navbar />
        <Form />
      </div>
    </div>
  );
}

export default App;
