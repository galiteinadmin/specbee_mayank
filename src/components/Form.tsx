import { makeStyles } from "@material-ui/core/styles";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Checkbox from "@mui/material/Checkbox";
import MenuItem from "@mui/material/MenuItem";
import OutlinedInput from "@mui/material/OutlinedInput";

import ListItemText from "@mui/material/ListItemText";
import React from "react";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import "./form.css";

const names = [
  "Oliver Hansen",
  "Van Henry",
  "April Tucker",
  "Ralph Hubbard",
  "Omar Alexander",
  "Carlos Abbott",
  "Miriam Wagner",
  "Bradley Wilkerson",
  "Virginia Andrews",
  "Kelly Snyder",
];

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: 48 * 4.5 + 8,
      width: 250,
    },
  },
};

const useStyles = makeStyles({
  formPage: {
    margin: "25px auto",
    width: "80%",
  },
  formControl: {
    // margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  root: {
    "& .form-container": {
      display: "flex",
      flexWrap: "wrap",
      gap: "20px",
      "& .inp": {
        width: "47%",
        "& .form-controls": {
          width: "100%",
        },
        "& > *": {
          marginTop: "15px",
          width: "100%",
          display: "flex",
        },
      },
    },
    "& .error": {
      borderColor: "red",
      color: "red",
    },
    "& > .save": {
      width: "auto",
      float: "right",
      margin: "30px",
      marginBottom: "25px",
    },
    "& .gender-courses": {
      marginTop: "25px",
      width: "100%",
      display: "flex",

      "& > *": {
        width: "100%",
        "& > *": {
          width: "94%",
        },
      },
    },
  },
});

function Form() {
  const classes = useStyles();
  const [personName, setPersonName] = React.useState<string[]>([]);

  const [address, setAddress] = React.useState("");

  const handleAddressChange = (event: SelectChangeEvent) => {
    setAddress(event.target.value as string);
  };

  const handleChange = (event: SelectChangeEvent<typeof personName>) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a the stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  return (
    <div className={classes.formPage}>
      <h2>Supplier Details</h2>
      <hr />
      <form
        className={classes.root}
        autoComplete="off"
        // onSubmit={submitHandler}
      >
        <div className="form-container">
          <div className="inp">
            <TextField
              id="name"
              label="Supplier Name"
              className="form-controls"
              variant="outlined"
            />
          </div>

          {/* Email Input */}
          <div className="inp">
            <FormControl sx={{ width: 300 }} variant="outlined">
              <InputLabel id="demo-multiple-checkbox-label">
                Unit / Warehouse
              </InputLabel>
              <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                value={personName}
                onChange={handleChange}
                input={<OutlinedInput />}
                renderValue={(selected) => selected.join(", ")}
                MenuProps={MenuProps}
              >
                {names.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox checked={personName.indexOf(name) > -1} />
                    <ListItemText primary={name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>

          <div className="inp">
            <TextField
              id="date"
              label="Transaction Date"
              type="date"
              defaultValue="2017-05-24"
              sx={{ width: 220 }}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>

          <div className="inp">
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">
                Supplier Address
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={address}
                label="Age"
                onChange={handleAddressChange}
              >
                <MenuItem value="New York">New York</MenuItem>
                <MenuItem value="India">India</MenuItem>
                <MenuItem value="USA">USA</MenuItem>
              </Select>
            </FormControl>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Form;
